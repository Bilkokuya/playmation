﻿using UnityEngine;
using System.Collections;

public class ObstacleCollide : MonoBehaviour {
	
	void OnTriggerEnter2D (Collider2D c)
	{
		if (c.gameObject.tag == "Player") 
		{
			c.gameObject.GetComponent<Death>().OnDeath();
		}
	}
}
