﻿using UnityEngine;
using System.Collections;

public class Death : MonoBehaviour 
{
	public bool mInvulnerable = false;
	public int respawns;

	[SerializeField] float invulnerabilityTime;
	[SerializeField] string prefabName;

	bool dying = false;
	float timer = 0.7f;
	
	// Update is called once per frame
	void Update () {

		if ( invulnerabilityTime > 0.0f )
		{
			invulnerabilityTime -= Time.deltaTime;
			mInvulnerable = true;
		}
		else
		{
			mInvulnerable = false;
		}

		if ( dying )
		{
			timer -= Time.deltaTime;
			if ( timer < 0.0f )
			{
				Destroy ( gameObject );
			}
		}
	}

	public void OnDeath()
	{
		if(!mInvulnerable)
		{
			GetComponent< PlayerControls >().enabled = false;
			GetComponent< PlaySound >().Play();
			GetComponent< PlayAnimations >().PlayDeath();

			dying = true;

			if ( respawns > 0 )
			{
				GameObject player = (GameObject) Instantiate( Resources.Load( prefabName ) );
				Vector3 position = transform.position;
				position.y = 0.0f;
				player.transform.position = position;
				player.GetComponent< Death >().respawns = respawns - 1;

				((CameraFollow)FindObjectOfType( typeof( CameraFollow ) )).player = player.transform;
			}
		}
	}
}