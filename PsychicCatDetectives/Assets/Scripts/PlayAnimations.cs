﻿using UnityEngine;
using System.Collections;

public class PlayAnimations : MonoBehaviour {

	[SerializeField] SpriteRenderer mainRenderer;
	[SerializeField] GameObject		angular;
	[SerializeField] GameObject		death;
	[SerializeField] GameObject		splat;
	bool isPlaying = false;

	float timer = 0;


	void Start()
	{
		angular.SetActive( false );
		death.SetActive( false );
		splat.SetActive( false );
	}

	void Update()
	{
		if ( isPlaying )
		{
			timer += Time.deltaTime;

			if( (!death.activeSelf && timer > 0.5f) || (timer > 0.7f) )
			{
				timer = 0;
				death.SetActive(false);
				splat.SetActive(false);
				angular.SetActive(false);
				mainRenderer.enabled = true;
				isPlaying = false;
			}
		}
	}

	public void PlaySplat()
	{
		isPlaying = true;
		mainRenderer.enabled = false;
		death.SetActive( false );
		angular.SetActive( false );
		splat.SetActive( true );
	}

	public void PlayAngular()
	{
		isPlaying = true;
		mainRenderer.enabled = false;
		death.SetActive( false );
		angular.SetActive( true );
		splat.SetActive( false );
	}

	public void PlayDeath()
	{
		isPlaying = true;
		mainRenderer.enabled = false;
		splat.SetActive( false );
		angular.SetActive( false );
		death.SetActive( true );
	}

}
