﻿using UnityEngine;
using System.Collections;

public class SpriteParticle : MonoBehaviour {
	
	public Vector3 	mDirection;
	public float 	mSpeed;
	public float 	mGrowth;
	public float	mRotation;
	
	// Update is called once per frame
	void Update () {
		transform.Translate( mDirection * mSpeed * Time.deltaTime );
		transform.RotateAround ( transform.localPosition, new Vector3( 0.0f, 0.0f, 1.0f ), mRotation * Time.deltaTime );
		transform.localScale += new Vector3( 1.0f, 1.0f, 0.0f ) * mGrowth * Time.deltaTime;
	}
}
