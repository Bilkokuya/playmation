﻿using UnityEngine;
using System.Collections;

public class ScrollingBackground : MonoBehaviour 
{
	Sprite sprite;
	public float mScrollingSpeed = 15.0f;
	public GameObject mFirstBG;
	public GameObject mSecondBG;
	public GameObject mThirdBG;
	public GameObject mFourthBG;

	float bgSizeX;

	// Use this for initialization
	void Start () 
	{
		bgSizeX = (mFirstBG.GetComponent<SpriteRenderer>().sprite.bounds.max.x * mFirstBG.transform.lossyScale.x) * 2;

		mFirstBG.transform.position = GameObject.FindGameObjectWithTag("MainCamera").transform.position;
		mSecondBG.transform.position = new Vector3(mFirstBG.transform.position.x + bgSizeX, mFirstBG.transform.position.y, mFirstBG.transform.position.z);
		mThirdBG.transform.position = new Vector3(mSecondBG.transform.position.x + bgSizeX, mFirstBG.transform.position.y, mFirstBG.transform.position.z);
		mFourthBG.transform.position = new Vector3(mThirdBG.transform.position.x + bgSizeX, mFirstBG.transform.position.y, mFirstBG.transform.position.z);
	}
	
	// Update is called once per frame
	void Update ()
	{
		mFirstBG.transform.position = new Vector3(mFirstBG.transform.position.x - mScrollingSpeed * Time.deltaTime, transform.position.y, transform.position.y);
		mSecondBG.transform.position = new Vector3(mSecondBG.transform.position.x - mScrollingSpeed * Time.deltaTime, transform.position.y, transform.position.y);
		mThirdBG.transform.position = new Vector3(mThirdBG.transform.position.x - mScrollingSpeed * Time.deltaTime, transform.position.y, transform.position.y);
		mFourthBG.transform.position = new Vector3(mFourthBG.transform.position.x - mScrollingSpeed * Time.deltaTime, transform.position.y, transform.position.y);

		if(mThirdBG.transform.position.x < GameObject.FindGameObjectWithTag("MainCamera").transform.position.x)
		{
			mFirstBG.transform.position = new Vector3(mFourthBG.transform.position.x + bgSizeX, mFourthBG.transform.position.y, mFourthBG.transform.position.z);
			GameObject temp1 = mFirstBG;
			//GameObject temp2 = mSecondBG;
			//GameObject temp3 = mThirdBG;
			//GameObject temp4 = mFourthBG;

			mFirstBG = mSecondBG;
			mSecondBG = mThirdBG;
			mThirdBG = mFourthBG;
			mFourthBG = temp1;
			//mSecondBG = temp;
		}
	}
}
