﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {
	public enum ScoreOwner {
		SCOREA,
		SCOREB,
		BOTH
	}

	[SerializeField] AudioBank	bank;
	[SerializeField] ScoreOwner scoreOwner;
	[SerializeField] float 		score;
	private ScoreManager 		scoreManager;

	void Start()
	{
		scoreManager = (ScoreManager) FindObjectOfType( typeof( ScoreManager ) );
	}

	void OnTriggerEnter2D( Collider2D collider )
	{
		if ( collider.CompareTag( "Player" ) ) {

			PlayAnimations anims = collider.GetComponent< PlayAnimations >();
			GetComponent< PlaySound >().Play();
			BgAnimationSpawner spawner = (BgAnimationSpawner)FindObjectOfType( typeof( BgAnimationSpawner ) );
			spawner.SpawnAnimation( GetComponent< SpriteRenderer >().color );

			switch( scoreOwner )
			{
			case ScoreOwner.SCOREA:
				scoreManager.ScoreA += score;
				anims.PlayAngular();
				break;

			case ScoreOwner.SCOREB:
				scoreManager.ScoreB += score;
				anims.PlaySplat();
				break;

			case ScoreOwner.BOTH:
				scoreManager.ScoreA += score;
				scoreManager.ScoreB += score;
				break;
			}
			Destroy( gameObject );		
		}
	}
}