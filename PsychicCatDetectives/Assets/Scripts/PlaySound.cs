﻿using UnityEngine;
using System.Collections;

public class PlaySound : MonoBehaviour {

	[SerializeField] private AudioClip[] clips;

	public void Play()
	{
		((AudioSource)FindObjectOfType(typeof(AudioSource))).PlayOneShot( clips[ Random.Range( 0, clips.Length ) ], 1.0f );
	}
}
