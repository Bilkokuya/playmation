﻿using UnityEngine;
using System.Collections;

public class RandomPrefab : MonoBehaviour {

	public Color color = new Color( 0.0f, 0.0f, 0.0f );
	[SerializeField] private string[] prefabNames;

	// Use this for initialization
	void Start () {
		GameObject prefab = (GameObject) Instantiate( Resources.Load( prefabNames[ Random.Range ( 0, prefabNames.Length ) ] ) );
		prefab.transform.position 	= transform.position;
		prefab.transform.rotation 	= transform.rotation;
		prefab.transform.localScale = transform.localScale;
		prefab.transform.parent 	= transform.parent;

		//badly hacked code for setting the color (when needed)
		if ( color != new Color( 0.0f, 0.0f, 0.0f ) ) {
			SpriteRenderer spriteRenderer = prefab.GetComponent< SpriteRenderer >();
			if ( spriteRenderer )
			{
				spriteRenderer.color = color;
			}
		}

		Destroy ( gameObject );
	}
}
