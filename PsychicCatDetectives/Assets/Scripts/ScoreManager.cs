﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	private float scoreA;
	private float scoreB;

	/* Accessor for the score of the top player */
	public float ScoreA {
		get {
			return scoreA;
		}
		set {
			scoreA = value;
		}
	}

	/* Accessor for the score of the bottom player */
	public float ScoreB {
		get {
			return scoreB;
		}
		set {
			scoreB = value;
		}
	}
}
