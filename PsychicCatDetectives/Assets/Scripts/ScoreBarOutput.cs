﻿using UnityEngine;
using System.Collections;

public class ScoreBarOutput : MonoBehaviour {
	[SerializeField] private GameObject scoreA;
	[SerializeField] private GameObject scoreB;
	[SerializeField] private float scoreScale;
	[SerializeField] private float maxScale;
	private ScoreManager scoreManager;

	void Start()
	{
		scoreManager = ( ( ScoreManager  )FindObjectOfType( typeof( ScoreManager ) ) );
	}

	void Update()
	{
		float scaleA = scoreManager.ScoreA * scoreScale;
		float scaleB = scoreManager.ScoreB * scoreScale;
		scoreA.transform.localScale = new Vector3( 1.0f, scaleA,  1.0f );
		scoreB.transform.localScale = new Vector3(  1.0f, scaleB,  1.0f );
	}
}
