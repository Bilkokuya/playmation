﻿using UnityEngine;
using System.Collections;

public class AudioBank : MonoBehaviour {

	[SerializeField] private AudioClip[] clips;

	public AudioClip GetClip()
	{
		return clips[ Random.Range( 0 , clips.Length ) ];
	}
}
