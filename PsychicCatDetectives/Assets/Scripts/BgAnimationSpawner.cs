﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BgAnimationSpawner : MonoBehaviour 
{
	[SerializeField] private string[]	mPrefabNames;
	[SerializeField] private Vector4	mSpawnerOffsetBounds;	// minX,maxX,minY,maxY
	[SerializeField] private GameObject mSpawnerPosition;
	[SerializeField] private int[] 		mMaxAnimsPerPhase;
	[SerializeField] private int 		mHighestPhaseNumber; 	// highest phase number used in the scene, i.e. Phase1, Phase2 etc Makes startup faster, not necessary
	[SerializeField] private Vector2[]	mRangeScales;			// scale offset range (min,max) for each phase (1,1) for no change
	[SerializeField] private Vector2[]	mRangeRotations;		// rotation offset range (min, max) for each phase (0,0) for no change

	private	List< List< GameObject > > 	mPhasePoints; 			// list of lists of phase points
	private float 						mAnimationZ;
	private float						mAnimationZDelta = -0.001f;
	private int							mCurrentPhase;
	private int							mNumberSpawnedThisPhase;
	
	void Awake ()
	{
		mPhasePoints = new List< List< GameObject > >();
	}

	// Use this for initialization
	void Start () 
	{
		/* find all phase tagged objects and add them to their correct list */
		int i = 0;
		while( i < mHighestPhaseNumber )
		{
			++i;

			GameObject[] phasePoints = GameObject.FindGameObjectsWithTag( "Phase" + i );
			if ( phasePoints.Length > 0  )
			{
				mPhasePoints.Add( new List< GameObject >( phasePoints ) );
			}
			else
			{
				break;
			}
		}
	}

	public void SpawnAnimation( Color color )
	{
		/* position the comparison point randomly in it's bounds */
		Vector3 offset = new Vector3( Random.Range( mSpawnerOffsetBounds.x, mSpawnerOffsetBounds.y ), Random.Range( mSpawnerOffsetBounds.z, mSpawnerOffsetBounds.w ) );
		Vector3 comparisonPosition = mSpawnerPosition.transform.position + offset;

		/* invert the y bounds so the next one spawns on the other side */
		mSpawnerOffsetBounds.z *= -1;
		mSpawnerOffsetBounds.w *= -1;

		/* while there are points to spawn in */
		if ( mPhasePoints.Count > 0 )
		{
			List< GameObject > phasePoints = mPhasePoints[ 0 ];

			/* compare each point in this phase, distance wise, to the comparison/spawn point location 
			 	the nearest point will is chosen*/
			float closestDistance = Mathf.Infinity;
			GameObject closestPoint = null;
			foreach( GameObject phasePoint in phasePoints )
			{
				float distance = ( comparisonPosition - phasePoint.transform.position ).magnitude;

				if ( closestPoint == null 
				    || distance < closestDistance )
				{
					closestPoint = phasePoint;
					closestDistance = distance;
				}
			}

			int phaseIndex = System.Math.Min( mCurrentPhase, mPrefabNames.Length-1 );

			/* ensure new animation is in front of last one */
			mAnimationZ += mAnimationZDelta;

			/* spawn the animation */
			GameObject animation = (GameObject) Instantiate( Resources.Load( mPrefabNames[ phaseIndex ] ) );
			animation.transform.position = closestPoint.transform.position + new Vector3( 0.0f, 0.0f, mAnimationZ );
			animation.transform.rotation = closestPoint.transform.rotation;
			animation.transform.parent = closestPoint.transform;

			/* alter the size/rotation of the animation */
			float rotationOffset = Random.Range( mRangeRotations[ phaseIndex ].x, mRangeRotations[ phaseIndex ].y );
			float scaleOffset = Random.Range( mRangeScales[ phaseIndex ].x, mRangeScales[ phaseIndex ].y );
			animation.transform.localScale = new Vector3( scaleOffset, scaleOffset, 1.0f );
			animation.transform.Rotate( new Vector3(0.0f, 0.0f, rotationOffset ) );

			/* set the color of the animation */
			animation.GetComponent< RandomPrefab >().color = color;

			phasePoints.Remove( closestPoint );

			mNumberSpawnedThisPhase++;

			/* move to next phase if enough spawned, or no locations are free */
			if ( phasePoints.Count == 0 
			    || mNumberSpawnedThisPhase >= mMaxAnimsPerPhase[ mCurrentPhase ] )
			{
				mPhasePoints.RemoveAt( 0 );
				mCurrentPhase++;
				mNumberSpawnedThisPhase = 0;
			}
		}
	}
}
