﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour 
{
	[SerializeField] private float movementSpeed;
	[SerializeField] private float controlSpeed;
	[SerializeField] private string buttonA;
	[SerializeField] private string buttonB;
	[SerializeField] private SpriteParticleEmitter emitterUp;
	[SerializeField] private SpriteParticleEmitter emitterDown;

	/* handle the player movement */
	void Update () 
	{
		if ( Input.GetButton( buttonA ) && Input.GetButton( buttonB ) )
		{
		}
		else if ( Input.GetButton( buttonA ) )
		{
			rigidbody2D.AddForce( new Vector2( 0.0f, controlSpeed ) );
		}
		else if ( Input.GetButton ( buttonB ) )
		{
			rigidbody2D.AddForce( new Vector2( 0.0f, -controlSpeed ) );
		}

		rigidbody2D.AddForce( new Vector2( movementSpeed, 0.0f ) );

		if ( Input.GetButton( buttonA ) )
		{
			emitterDown.enabled = true;
		}
		else
		{
			emitterDown.enabled = false;
		}

		if ( Input.GetButton( buttonB ) )
		{
			emitterUp.enabled = true;
		}
		else
		{
			emitterUp.enabled = false;
		}

	}
}
