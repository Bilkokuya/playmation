﻿using UnityEngine;
using System.Collections;

public class GrowFromSpawn : MonoBehaviour {

	[SerializeField] float startSize;
	[SerializeField] float endSize;
	[SerializeField] float growTime;

	// Use this for initialization
	void Start () {
		StartCoroutine( "RoutineGrow" );
	}

	IEnumerator RoutineGrow()
	{
		float timer = 0.0f;
		while( timer < growTime )
		{
			timer += Time.deltaTime;
			float t = timer / growTime;
			transform.localScale = new Vector3( t, t, 1.0f );
			yield return null;
		}
	}
}
