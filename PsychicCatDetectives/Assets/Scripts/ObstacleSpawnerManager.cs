﻿using UnityEngine;
using System.Collections;

public class ObstacleSpawnerManager : MonoBehaviour 
{
	public string[] mObstacleNames;
	public string pickupPrefabName;
	public float mMinTimeToObstacle = 1.0f;
	public float mMaxTimeToObstacle = 4.0f;
	public float mMinSpawnInterval = 0.5f;
	public float topOffset = 2;
	public float bottomOffset = 2;
	bool spawnedPickup = false;

	float gapDistance = 0;

	float timer = 0;
	float mSpawnInterval = 0;

	private Transform[] transformChildren;

	// Use this for initialization
	void Start () 
	{
		transformChildren = GetComponentsInChildren<Transform>();
		GenerateSpawnInterval();
	}

	void GenerateSpawnInterval()
	{
		mSpawnInterval = Mathf.Max(Random.Range(mMinTimeToObstacle, mMaxTimeToObstacle) + mMinSpawnInterval, mMaxTimeToObstacle);
		gapDistance = Random.Range(0.5f, 2.0f);
		gapDistance *= Random.Range(1, 10) > 5 ? 1 : -1;
	}
	
	// Update is called once per frame
	void Update () 
	{
		timer += Time.deltaTime;

		/* spawn pickups in the middle of each set of obstacles*/
		if ( ( timer > ( mSpawnInterval / 2.0f ) ) && !spawnedPickup )
		{
			spawnedPickup = true;
			GameObject pickup = (GameObject) Instantiate( Resources.Load( pickupPrefabName ) );
			pickup.transform.position = new Vector3( transformChildren[1].position.x, Random.Range( -bottomOffset, topOffset ), 0.0f );
		}

		/* spawn obstacles every interval */
		if(timer >= mSpawnInterval)
		{
			timer = 0;
			spawnedPickup = false;
			GenerateSpawnInterval();

			int obstacleIndex = Random.Range( 0, mObstacleNames.Length );

			/* bottom */					   Instantiate( Resources.Load( mObstacleNames[obstacleIndex] ), new Vector3(transformChildren[2].position.x, transformChildren[2].position.y - bottomOffset + gapDistance, transformChildren[2].position.z), transformChildren[1].rotation);
			GameObject top 		= (GameObject) Instantiate( Resources.Load( mObstacleNames[obstacleIndex] ), new Vector3(transformChildren[1].position.x, transformChildren[1].position.y + topOffset + gapDistance, transformChildren[1].position.z), transformChildren[0].rotation);

			top.transform.localScale = new Vector3( top.transform.localScale.x, -top.transform.localScale.y, top.transform.localScale.z );
		}
	}
}
