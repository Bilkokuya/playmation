﻿using UnityEngine;
using System.Collections;

public class AnimatorTest : MonoBehaviour {

	private Animator anim;
	float timer;
	private AnimatorStateInfo currentBaseState;

	// Use this for initialization
	void Start () 
	{
		anim = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		currentBaseState = anim.GetCurrentAnimatorStateInfo(0);
		print(currentBaseState.IsName("AngularMorph"));
		timer += Time.deltaTime;

		gameObject.GetComponent<Animator>().SetBool("AngularAnim", false);

		if(timer > 5)
		{
			timer = 0;
			gameObject.GetComponent<Animator>().SetBool("AngularAnim", true);
			//gameObject.GetComponent<Animator>().SetBool("AngularAnim", false);
		}
	}
}
