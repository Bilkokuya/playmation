﻿using UnityEngine;
using System.Collections;

public class AutoMove : MonoBehaviour 
{
	public float mSpeed = 0.1f;
	public int mDirection = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		float speed = transform.position.x + mSpeed * mDirection;
		transform.position = new Vector3(speed * FindObjectOfType<GameConstants>().mGameSpeed, transform.position.y, transform.position.z);
	}
}
