﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpriteParticleEmitter : MonoBehaviour {
	
	public float	mSpawnRate;
	public Vector3 	mDirection;
	public float 	mDeviance;
	public float 	mSpeed;
	public float 	mGrowth;
	public float	mRotation;
	
	[SerializeField] int			mMaxParticles;
	[SerializeField] private string mPrefabName;

	private Queue< SpriteParticle > mActiveParticles 	= new Queue< SpriteParticle >();
	private Queue< SpriteParticle > mDisabledParticles 	= new Queue< SpriteParticle >();

	private float timer;

	void Awake() {
		/* initialise the disabled particles */
		for( int i = 0; i < mMaxParticles; ++i )
		{
			GameObject prefab = (GameObject) Instantiate( Resources.Load( mPrefabName ) );
			prefab.SetActive( false );
			mDisabledParticles.Enqueue( prefab.GetComponent< SpriteParticle >() );
		}
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;

		float spawnTime = 1.0f / mSpawnRate;
		while( timer > spawnTime )
		{
			timer -= spawnTime;

			SpawnParticle();
		}
	}

	void SpawnParticle()
	{
		SpriteParticle particle = null;

		/* when all particles are active take them from the front, and respawn them */
		if ( mDisabledParticles.Count == 0 )
		{
			particle = mActiveParticles.Dequeue();
		}
		else
		{
			particle = mDisabledParticles.Dequeue();
		}

		particle.transform.position = transform.position;
		Vector3 perpindicularDirection = Vector3.Cross( mDirection, new Vector3( 0.0f, 0.0f, 1.0f ) ).normalized;
		float directionRandom = Random.Range( -mDeviance, mDeviance );

		particle.mDirection = ( (1.0f - directionRandom ) * mDirection ) + ( directionRandom * perpindicularDirection );
		particle.mGrowth = Random.Range( -mDeviance, mDeviance ) * mGrowth + mGrowth;
		particle.mRotation = Random.Range( -mDeviance, mDeviance ) * mRotation + mRotation;
		particle.mSpeed = Random.Range( -mDeviance, mDeviance ) * mSpeed + mSpeed;

		particle.gameObject.SetActive( true );
		particle.enabled = true;

		mActiveParticles.Enqueue( particle );
	}
}
