﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour 
{
	public float camOffset = 0.0f;
	public float xMargin = 1.0f;		// Distance in the x axis the player can move before the camera follows.
	public float xSmooth = 8.0f;		// How smoothly the camera catches up with it's target movement in the x axis.	
	
	public Transform player;		// Reference to the player's transform.
	
	void Awake ()
	{
		// Setting up the reference.
		player = GameObject.FindGameObjectWithTag("Player").transform;
	}

	
	bool CheckXMargin()
	{
		// Returns true if the distance between the camera and the player in the x axis is greater than the x margin.
		return Mathf.Abs(transform.position.x - player.position.x) > xMargin;
	}
	

	void Update()
	{
		/* track the player while they are still alive */
		if ( player )
		{
			transform.position = new Vector3(player.transform.position.x + camOffset, 0, transform.position.z);
		}
	}
	
	
	void TrackPlayer ()
	{
		float targetX = transform.position.x;

		// If the player has moved beyond the x margin...
		if(CheckXMargin())
		{
			// ... the target x coordinate should be a Lerp between the camera's current x position and the player's current x position.
			targetX = Mathf.Lerp(transform.position.x, player.transform.position.x, xSmooth * Time.deltaTime);
			//targetX = player.position.x + camOffset;

			if (targetX < 0)
				targetX = 0;

		}

		// Set the camera's position to the target position with the same z component.
		transform.position = new Vector3(targetX, 0, transform.position.z);
	}
}
